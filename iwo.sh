#!/bin/bash
# by Arjan Schelhaas
# Provide filename as command line argument.

# Dataset for the amount of VWO graduates
if [ ! -f vwo_in_wo.csv ]; then
    wget -O vwo_in_wo.csv "https://www.onderwijsincijfers.nl/binary/andere-hoofdrubrieken/kengetallen/wetenschappelijk-onderwijs/deelnemerswo/directe-en-indirecte-instroom-wo/index/Aantallen-studenten-directe-en-indirecte-instroom-in-het-wo--naar-vooropleiding.csv"
fi

if [ ! -f vwo_in_hbo.csv ]; then
    wget -O vwo_in_hbo.csv "https://www.onderwijsincijfers.nl/binary/andere-hoofdrubrieken/kengetallen/hoger-beroepsonderwijs/deelnemers-hbo/directe-en-indirecte-instroom-hbo/index/HBOaantalleninstroom2016.csv"
fi

# Datasets for VWO diploma's
if [ ! -f vwo_data13.csv ]; then
    wget -O vwo_data13.csv "https://www.duo.nl/open_onderwijsdata/images/07.-geslaagden%2C-gezakten-en-cijfers-2012-2013.csv"
fi

if [ ! -f vwo_data14.csv ]; then
    wget -O vwo_data14.csv "https://www.duo.nl/open_onderwijsdata/images/07.-geslaagden%2C-gezakten-en-cijfers-2013-2014.csv"
fi

if [ ! -f vwo_data15.csv ]; then
    wget -O vwo_data15.csv "https://www.duo.nl/open_onderwijsdata/images/07.-geslaagden%2C-gezakten-en-cijfers-2014-2015.csv"
fi

if [ ! -f vwo_data16.csv ]; then
    wget -O vwo_data16.csv "https://www.duo.nl/open_onderwijsdata/images/07.-geslaagden%2C-gezakten-en-cijfers-2015-2016.csv"
fi

# Extraction of datasets
./extract_data.py vwo_in_wo.csv vwo_in_hbo.csv vwo_data13.csv vwo_data14.csv vwo_data15.csv vwo_data16.csv
