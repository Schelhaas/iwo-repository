IWO research
======
25-03-2018
By A.J. Schelhaas

Within this repository you will find a shell script and a python script.
The shell script will download the datasets used for this research and run the python script.
The Python script will print the data necessary for this research in the terminal.

Usage:
Use command './iwo.sh' to start the bash script.