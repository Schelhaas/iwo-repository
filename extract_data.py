#!/usr/bin/python3
# File name: extract_data.py
# Extracts data from .csv file for IWO research.
# Author: A.J. Schelhaas
# 16-03-2018

import sys


def get_studying_graduates(path_wo,path_hbo):

    studying_graduates_wo = []
    studying_graduates_hbo = []
    studying_graduates_total = []
    
    with open(path_wo, 'rt', encoding='latin-1') as file_handle:
        for line in file_handle:
            fields = line.split(';')
            if fields[1] != 'vwo direct':
                fields[1] = fields[1].replace(',','.')
                studying_graduates_wo += [float(fields[1])*1000]
                print(float(fields[1])*1000)
            
    with open(path_hbo, 'rt', encoding='latin-1') as file_handle:
        for line in file_handle:
            fields = line.split(';')
            if fields[3] != 'vwo':
                fields[3] = fields[3].replace(',','.')
                studying_graduates_hbo += [float(fields[3])*1000]
                print(float(fields[3])*1000)
    
    studying_graduates_total.append(int(studying_graduates_wo[7] + studying_graduates_hbo[9]))
    studying_graduates_total.append(int(studying_graduates_wo[8] + studying_graduates_hbo[10]))
    studying_graduates_total.append(int(studying_graduates_wo[9] + studying_graduates_hbo[11]))
    studying_graduates_total.append(int(studying_graduates_wo[10] + studying_graduates_hbo[12]))
    
    return studying_graduates_total

def get_vwo(path):
    vwo_diploma = 0

    with open(path, 'rt', encoding='latin-1') as file_handle:
        for line in file_handle:
            fields = line.split(';')
            if fields[0] != '' and fields[0] != 'BRIN NUMMER' and fields[5] == 'VWO':
                vwo_diploma += int(fields[10])

    return vwo_diploma


def main(argv):
    print('Bachelor data:')
    bachelor_amount = get_studying_graduates(argv[1],argv[2])
    for i in range(4):
        print('20' + str(13+i) + ':', bachelor_amount[i])

    print()

    print('VWO data:')
    vwo_amount = []
    for i in range(4):
        vwo_amount += [get_vwo(argv[i + 3])]
        print('20' + str(13+i) + ':', vwo_amount[i])
        
    print()
        
    print('{0:<24} {1:<24} {2}'.format('Year:', 'VWO graduates studying:', 'VWO diploma amount:'))    
    print('{0:<24} {1:<24} {2}'.format('2013 & 2014:', bachelor_amount[0] + bachelor_amount[1], vwo_amount[0] + vwo_amount[1]))
    print('{0:<24} {1:<24} {2}'.format('2015 & 2016:', bachelor_amount[2] + bachelor_amount[3],vwo_amount[2] + vwo_amount[3]))


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv)
    else:
        print('Usage: Provide filename as command line argument')
